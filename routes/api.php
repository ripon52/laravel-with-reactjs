<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post("/store-category",'Api\CategoryController@store');
Route::get("all-categories",'Api\CategoryController@index');
Route::post('/destroy/category/{id}','Api\CategoryController@destroy');
Route::get('/category/edit/{id}','Api\CategoryController@edit');
Route::post('/update/category/{id}','Api\CategoryController@update');
Route::post('/upload-file','Api\CategoryController@uploadFile');


