<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        return  Category::query()->paginate(2);

    }

    public function store(Request $request){
        $name = $request->name;
        Category::create(['name'=>$name]);
        return json_encode(['success'=>1]);
    }

    public function destroy(Request $request,$id){
        Category::query()->findOrFail($id)->delete();
        return response(['success'=>1]);
    }

    public function edit($id){
        return Category::query()->findOrFail($id);
        //return $category;
    }

    public function update(Request $request,$id){
        Category::query()->findOrFail($id)->update($request->all());
    }

    public function uploadFile(Request $request){
        $data = $request->except('image');
        if ($request->file('image')){
            $image = $request->file('image');
            $fileName = Date("Y")."_".substr(md5(time()),0,10).".".$image->getClientOriginalExtension();
            $image->move(public_path('upload/'),$fileName);
            $data['image'] = $fileName;
        }
        Image::query()->create($data);
        return response(['success'=>1]);
    }

}
