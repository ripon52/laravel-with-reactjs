import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {BrowserRouter as Router,Link,Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./Header";

export default class Index extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <h3 className="bg-info text-center" style={{"padding":"5px 10px"}}> Laravel with React Crud.</h3>
                <Header />
            </div>
        );
    }
}

if (document.getElementById('index')) {
    ReactDOM.render( <Router>  <Index /> </Router> ,document.getElementById('index'));
}
