import React, {Component} from 'react';

class Error404 extends Component {
    render() {
        return (
            <div>
                <div className="alert-danger">
                    <h4 className="bg-danger text-center"> Error 404 Custom.</h4>
                </div>
            </div>
        );
    }
}

export default Error404;
