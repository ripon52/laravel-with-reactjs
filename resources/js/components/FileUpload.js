import React, {Component} from 'react';
import  axios from 'axios';
class FileUpload extends Component {

    constructor(props) {
        super(props);

        this.state={
            successAlert:0,
            file:'',
            fileUrl:""
        };
    //     bind Methods
        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }

    onSubmitForm(e){
        e.preventDefault();

        let formData = new FormData();
        let image = this.state.file;
        let name = "Ripon uddin";

        formData.append('image',image);
        formData.append('name',name);

        axios.post("/api/upload-file",formData).then(res=>{console.log(res)}).catch(err=>{console.log(err)});
    }


    onChangeName(e){

        let files = e.target.files[0];
        this.setState({file:files});

        let reader = new FileReader();

        reader.onload = function (ee) {

            document.getElementById("imageSrc").setAttribute("src",ee.target.result);

        };

        reader.readAsDataURL(e.target.files[0]);
    }


    render() {
        return (
            <div>

                <div className="alert alert-success" style={ this.state.successAlert != 0 ? {"display":"block"} :{"display":"none"} }>
                    <strong className="data-close"> Category Inserted successfully</strong>
                </div>

                <div>
                    <h5 className="bg-info text-center"> Selected Image : </h5>
                    <img src="" id="imageSrc"  alt="No Image"/>
                </div>

                <form onSubmit={this.onSubmitForm} encType={"multipart/form-data"}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Image Select</label>
                        <input type="file" name="image" accept="image/*" onChange={this.onChangeName}  className="form-control"   />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>

                <hr/>
            </div>
        );
    }
}


export default FileUpload;
