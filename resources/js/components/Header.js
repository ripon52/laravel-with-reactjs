import React, {Component} from 'react';
import {BrowserRouter as Router,Link,Route,Switch} from 'react-router-dom';

import Home from "./Home";
import About from "./About";
import Nav from "./Nav";
import Index from "./category/Index";
import EditCategory from "./category/EditCategory";

import FileUpload from "./FileUpload";
import Error404 from "./Error404";

class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (

            <Router>
                <div>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <a className="navbar-brand" href="#">Navbar</a>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link to="/" className="nav-link" >Home</Link>

                                </li>
                                <li className="nav-item">
                                    <Link to="/about" className="nav-link">About Us</Link>
                                </li>

                                <li className="nav-item">
                                    <Link to="/category" className="nav-link">Category</Link>
                                </li>

                                <li className="nav-item">
                                    <Link to="/upload" className="nav-link">File Upload</Link>
                                </li>

                            </ul>
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search" onChange={this.searchData} />
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </nav>
                </div>
                <Switch>
                    <Route exact path="/upload" component={FileUpload}/>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/about" component={About}/>
                    <Route exact path="/category" component={Index}/>
                    <Route exact path="/:id/edit" component={EditCategory} />
                    <Route exact path="/*" component={Error404} />
                </Switch>
            </Router>
        );
    }
}

export default Header;
