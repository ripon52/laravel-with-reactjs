import React, {Component} from 'react';
import {BrowserRouter as Router,Link,Route} from 'react-router-dom';
import Home from "./Home";
import About from "./About";
import Index from "./category/Index";
import EditCategory from "./category/EditCategory";

class Nav extends Component {

    constructor() {
        super();
        this.state={
            name:""
        }
        this.searchData = this.searchData.bind(this);
    }

    searchData(e){
        this.setState({name:e.target.value});
        console.log(e.target.value);
        this.props.searchKeyword = this.state.name;
    }

    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <a className="navbar-brand" href="#">Navbar</a>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link to="/" className="nav-link" >Home</Link>

                                </li>
                                <li className="nav-item">
                                    <Link to="/about" className="nav-link">About Us</Link>
                                </li>

                                <li className="nav-item">
                                    <Link to="/category" className="nav-link">Category</Link>
                                </li>

                            </ul>
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search" onChange={this.searchData} />
                                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </nav>
                </div>

                <Route exact path="/" component={Home}/>
                <Route exact path="/about" component={About}/>
                <Route exact path="/category" component={Index}/>
                <Route exact path="/:id/edit" component={EditCategory} />
            </Router>
        );
    }
}

export default Nav;
