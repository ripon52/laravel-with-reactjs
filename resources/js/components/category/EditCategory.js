import React, {Component} from 'react';
import  axios from 'axios';
import Listing from "./Listing";
class EditCategory extends Component {

    constructor(props) {
        super(props);
        this.state={
            name:'',
            categories:[],
            successAlert:0,
        };
        this.onSubmitForm = this.onSubmitForm.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
    }
// Update kaj kora hoynee..
    onSubmitForm(e){
        e.preventDefault();

        const data ={name:this.state.name};
        const id = this.props.match.params.id;

        axios.post('http://127.0.0.1:8000/api/update/category/'+id,data)
            .then(response=>{
                this.setState({
                    successAlert:1
                });
                
                this.props.history.push("/category");
            })
            .catch(error=>{

            });
    }

    onChangeName(e){
        var name = e.target.value;
        this.setState({
            name:name
        });
        console.log(name);
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/category/edit/'+this.props.match.params.id)
            .then(response=>{
                this.setState({
                    name:response.data.name,
                    updateId:response.data.id
                });
                console.log(response.data.name+" Update id : "+response.data.id);
            })
            .catch();
    }

    render() {
        return (
            <div>

                {
                    this.state.successAlert == 1
                    ?
                      <div className="alert alert-success">
                        <strong className="data-close"> Category Update successfully</strong>
                      </div>
                    :
                    null
                }


                <form onSubmit={this.onSubmitForm}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Category Name</label>
                        <input type="text" onChange={this.onChangeName} value={this.state.name} className="form-control"  placeholder="Enter Category Name" />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>

                <hr/>

            </div>
        );
    }
}

export default EditCategory;
