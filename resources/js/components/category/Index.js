import React, {Component} from 'react';
import {BrowserRouter as Router,Link,Route} from 'react-router-dom';
import Add from  './Add'
import Listing from  './Listing'
import EditCategory from "./EditCategory";

class Index extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <Router>
                <div>
                    <Link to="/category" className="btn btn-info m-3"> Listing</Link>
                    <Link to="/category/new-category" className="btn btn-info m-3"> New Category</Link>
                </div>

                <Route exact path="/category" component={Listing}/>
                <Route exact path="/category/new-category" component={Add}/>
                <Route exact path="/:id/edit" component={EditCategory}/>
            </Router>
        );
    }
}

export default Index;
