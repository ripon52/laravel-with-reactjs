import React, {Component} from 'react';
import {Link} from "react-router-dom";
import  axios from 'axios'
import Pagination from "react-js-pagination";

import ReactPaginate from 'react-paginate';

class Listing extends Component {

    constructor(props) {
        super(props);
        this.state={
            categories:[],
            activePage:1,
            itemsCountPerPage:1,
            totalItemsCount:1,
        };
     //   this.getData = this.getData.bind(this);
        this.deleteData = this.deleteData.bind(this);

        this.handlePageChange = this.handlePageChange.bind(this);

    }

   /* getData(){
        axios.get("http://127.0.0.1:8000/api/all-categories")
            .then(response=>{
                this.setState({categories:response.data.data})
            })
            .catch();
    }*/

    deleteData(id){
        axios.post("http://127.0.0.1:8000/api/destroy/category/"+id)
            .then(response=>{
                //this.setState({categories:response.data})
                var data =  this.state.categories;
                var i=0;
                for(i;i<data.length;i++){
                    if (data[i].id == id){
                        data.splice(i,1)
                    }
                }
                this.setState({categories:data});
            })
            .catch();
    }

    componentDidMount() {
        axios.get("http://127.0.0.1:8000/api/all-categories")
            .then(response=>{
                this.setState({
                    categories:response.data.data,
                    totalItemsCount:response.data.total,
                    itemsCountPerPage:response.data.per_page,
                    activePage: response.data.current_page
                })
            })
            .catch();
    }


    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        let url = "http://127.0.0.1:8000/api/all-categories?page="+pageNumber;
        axios.get(url)
            .then(response=>{
                this.setState({
                    categories:response.data.data,
                    totalItemsCount:response.data.total,
                    itemsCountPerPage:response.data.per_page,
                    activePage: response.data.current_page
                })
            })
            .catch();

    }



    render() {
        console.log(this.props);

        return (
            <div>
                <div className="container card">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Index</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Create Time </th>
                                    <th scope="col">Manage</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.state.categories.map((data,index)=>
                                    <tr key={data.id}>
                                        <th scope="row">{index}</th>
                                        <td>{data.name}</td>
                                        <td>{data.created_at}</td>
                                        <td>
                                            <Link className="btn btn-sm btn-success" to={`/${data.id}/edit`}>
                                                Edit
                                            </Link>

                                            <button className="btn btn-danger m-2" onClick={this.deleteData.bind(this,data.id)} type="button">Delete</button>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>


                        <div className="pagination justify-content-center">
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.itemsCountPerPage}
                                totalItemsCount={this.state.totalItemsCount}
                                pageRangeDisplayed={this.state.itemsCountPerPage}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass='page-item'
                                linkClass='page-link'
                            />
                        </div>




                    </div>
                </div>

            </div>
        );
    }
}

export default Listing;
