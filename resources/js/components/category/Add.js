import React, {Component} from 'react';
import axios from 'axios'

class Add extends Component {

    constructor() {
        super();
        this.state={
            name:"",
            successAlert:0,
            categories:[]
        }

        this.onChangeName = this.onChangeName.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }

    onSubmitForm(e){
        e.preventDefault();

        const name ={name:this.state.name};

        axios.post('http://127.0.0.1:8000/api/store-category',name)
            .then(response=>{
                this.setState({
                    successAlert:1
                });
                this.getData();
                console.log(response.data);
            })
            .catch(error=>{

            });
    }

    getData(){
        axios.get("http://127.0.0.1:8000/api/all-categories")
            .then(response=>{
                this.setState({categories:response.data})
            })
            .catch();
    }

    onChangeName(e){
        var name = e.target.value;
        this.setState({
            name:name
        });
        console.log(name);
    }
/*
    componentDidMount() {
        axios.get("http://127.0.0.1:8000/api/all-categories")
            .then(response=>{
                this.setState({categories:response.data})
            })
            .catch();
    }*/

    render() {
        return (
            <div>

                <div className="alert alert-success" style={ this.state.successAlert != 0 ? {"display":"block"} :{"display":"none"} }>
                    <strong className="data-close"> Category Inserted successfully</strong>
                </div>

                <form onSubmit={this.onSubmitForm}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Category Name</label>
                        <input type="text" onChange={this.onChangeName} value={this.state.name} className="form-control"  placeholder="Enter Category Name" />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>

                <hr/>



            </div>
    );
    }
    }

    export default Add;
